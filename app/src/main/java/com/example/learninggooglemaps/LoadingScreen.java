package com.example.learninggooglemaps;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

public class LoadingScreen extends AppCompatActivity {

    private static final long DELAY_TIME = 2000;

    public static void startLoadingScreen(Context context, Class<?> targetActivity) {
        Intent intent = new Intent(context, LoadingScreen.class);
        intent.putExtra("TARGET_ACTIVITY", targetActivity);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.loading_screen);

        ProgressBar progressBar = findViewById(R.id.progressBar);
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        progressAnimator.setDuration(DELAY_TIME);
        progressAnimator.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Class<?> targetActivity = (Class<?>) getIntent().getSerializableExtra("TARGET_ACTIVITY");

                // Always launch SignUpPage after the loading screen
                Intent intent = new Intent(LoadingScreen.this, SignUpPage.class);
                startActivity(intent);

                finish(); // Finish the LoadingScreen activity
            }
        }, DELAY_TIME);
    }
}
