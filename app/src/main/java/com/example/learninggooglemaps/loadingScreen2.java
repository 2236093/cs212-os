package com.example.learninggooglemaps;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

public class loadingScreen2 extends AppCompatActivity {

    private Class<?> targetActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.loading_screen);

        ProgressBar progressBar = findViewById(R.id.progressBar);
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        progressAnimator.setDuration(1000);
        progressAnimator.start();

        // Get the target activity class from the intent extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            targetActivity = (Class<?>) extras.getSerializable("TARGET_ACTIVITY");
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (targetActivity != null) {
                    Intent intent = new Intent(loadingScreen2.this, targetActivity);
                    startActivity(intent);
                }
                finish();
            }
        }, 1000);
    }
}
