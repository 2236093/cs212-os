package com.example.learninggooglemaps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
public class SignUpPage extends AppCompatActivity {

    private EditText usernameEditText, emailEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        // Initialize EditText fields
        usernameEditText = findViewById(R.id.Username_id);
        emailEditText = findViewById(R.id.editTextTextEmailAddress);
        passwordEditText = findViewById(R.id.editTextTextPassword);

        Button signUpButton = findViewById(R.id.Signup_button);
        TextView loginTxt = findViewById(R.id.login);
//
        TextView termServices = findViewById(R.id.Terms_services);

        termServices.setMovementMethod(LinkMovementMethod.getInstance());



        loginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSignIn();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                if (username.isEmpty() || email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(SignUpPage.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Save user data to SharedPreferences
                saveUserDataToSharedPreferences(username, email, password);

                // Go to the login page
                launchSignIn();
            }
        });
    }

    private void saveUserDataToSharedPreferences(String username, String email, String password) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        // Save user data to SharedPreferences
        editor.putString("username", username);
        editor.putString("email", email);
        editor.putString("password", password);

        // Apply changes
        editor.apply();
    }

    private void launchSignIn() {
        Intent intent = new Intent(this, LoginPage.class);
        startActivity(intent);
    }
}
