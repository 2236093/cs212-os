package com.example.learninggooglemaps;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private PopupWindow popUpWindow;

    private SharedPreferences wmbPreference1;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ImageView profileAccont = findViewById(R.id.profile_icon);

        // Initialize SharedPreferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);



        profileAccont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindowProfile(v);
            }
        });
    }

    private void showPopupWindowProfile(View anchorView) {
        View popUpView = LayoutInflater.from(this).inflate(R.layout.profile_main, null);

        popUpWindow = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);



        // Retrieve user data from SharedPreferences
        String storedUsername = preferences.getString("username", "");
        String storedEmail = preferences.getString("email", "");

        // Display user data in the PopupWindow
        TextView usernameTextView = popUpView.findViewById(R.id.usernameTextView);
        TextView emailTextView = popUpView.findViewById(R.id.emailTextView);

        usernameTextView.setText("Username: " + storedUsername);
        emailTextView.setText("Email: " + storedEmail);


        ImageView exitButton = popUpView.findViewById(R.id.exitButtonImageView2);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });
        Button logoutButton = popUpView.findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchLoginPage();
            }
        });

        popUpWindow.showAtLocation(anchorView, Gravity.CENTER, 0, 0);
    }

    private void launchLoginPage() {
        Intent intent = new Intent(this, LoginPage.class);
        startActivity(intent);
    }
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        LatLng bakakeng = new LatLng(16.383378828027766, 120.5925528921621);
        LatLng gym2 = new LatLng(16.386397, 120.595221);
        LatLng gym3 = new LatLng(16.381835, 120.595589);

        LatLng gym4 = new LatLng(16.384307, 120.588435);

        LatLng gym5 = new LatLng(16.380638, 120.591318);

        map.getUiSettings().setCompassEnabled(false);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map.getUiSettings().setMyLocationButtonEnabled(false);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(16.383378828027766, 120.5925528921621))
                .zoom(16)
                .tilt(67f)
                .bearing(314)
                .build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        int markerSizeInDp = 65; // Set your desired marker size in dp

        Marker bakakengMarker = mMap.addMarker(new MarkerOptions()
                .position(bakakeng)
                .title("Threy'sGym")
                .icon(createMarkerIcon(R.drawable.gympin,
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density),
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density)))
                .anchor(0.5f, 0.5f) // Center the marker
                .infoWindowAnchor(0.5f, 0.5f) // Center the info window
                .rotation(0) // Set the rotation angle
                .zIndex(1.0f)); // Set the zIndex



        Marker gym2Marker = mMap.addMarker(new MarkerOptions()
                .position(gym2)
                .title("Gym2")
                .icon(createMarkerIcon(R.drawable.gympin,
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density),
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density)))
                .anchor(0.5f, 0.5f) // Center the marker
                .infoWindowAnchor(0.5f, 0.5f) // Center the info window
                .rotation(0) // Set the rotation angle
                .zIndex(1.0f)); // Set the zIndex

        Marker gym3Marker = mMap.addMarker(new MarkerOptions()
                .position(gym3)
                .title("Gym3")
                .icon(createMarkerIcon(R.drawable.gympin,
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density),
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density)))
                .anchor(0.5f, 0.5f) // Center the marker
                .infoWindowAnchor(0.5f, 0.5f) // Center the info window
                .rotation(0) // Set the rotation angle
                .zIndex(1.0f)); // Set the zIndex

        Marker gym4Marker = mMap.addMarker(new MarkerOptions()
                .position(gym4)
                .title("Gym4")
                .icon(createMarkerIcon(R.drawable.gympin,
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density),
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density)))
                .anchor(0.5f, 0.5f) // Center the marker
                .infoWindowAnchor(0.5f, 0.5f) // Center the info window
                .rotation(0) // Set the rotation angle
                .zIndex(1.0f)); // Set the zIndex

        Marker gym5Marker = mMap.addMarker(new MarkerOptions()
                .position(gym5)
                .title("Gym5")
                .icon(createMarkerIcon(R.drawable.gympin,
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density),
                        (int) (markerSizeInDp * getResources().getDisplayMetrics().density)))
                .anchor(0.5f, 0.5f) // Center the marker
                .infoWindowAnchor(0.5f, 0.5f) // Center the info window
                .rotation(0) // Set the rotation angle
                .zIndex(1.0f)); // Set the zIndex


        mMap.moveCamera(CameraUpdateFactory.newLatLng(bakakeng));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.equals(bakakengMarker)) {
                    // Handle marker click here if needed

                    // Start MainActivity with transition
                    mainActivity();

                    return true;
                }
                return false;
            }
        });
    }

    private BitmapDescriptor createMarkerIcon(int iconId, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), iconId);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return BitmapDescriptorFactory.fromBitmap(resizedBitmap);
    }



    private void mainActivity() {
        Intent intent = new Intent(this, loadingScreen2.class);
        intent.putExtra("TARGET_ACTIVITY", MainActivity.class);
        startActivity(intent);
    }

}

