package com.example.learninggooglemaps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
public class LoginPage extends AppCompatActivity {

    private EditText usernameEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_layout);

        // Initialize EditText fields
        usernameEditText = findViewById(R.id.Username_id); // Assuming this is the username field
        passwordEditText = findViewById(R.id.editTextTextPassword);

        // Initialize other views
        TextView signUpLink = findViewById(R.id.sign_up);
        Button btnSignIn = findViewById(R.id.Signin_button);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get user input
                String enteredUsername = usernameEditText.getText().toString();
                String enteredPassword = passwordEditText.getText().toString();

                // Check credentials against stored user data
                if (checkCredentials(enteredUsername, enteredPassword)) {
                    // Successful login, launch MapsActivity or any other activity
                    launchMapsActivity();
                } else {
                    Toast.makeText(LoginPage.this, "Invalid username or password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        signUpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSignUp();
            }
        });
    }

    private void launchMapsActivity() {
        Intent intent = new Intent(this, loadingScreen2.class);
        intent.putExtra("TARGET_ACTIVITY", MapsActivity.class);
        startActivity(intent);
    }

    private void launchSignUp() {
        Intent intent = new Intent(this, SignUpPage.class);
        startActivity(intent);
    }

    private boolean checkCredentials(String enteredUsername, String enteredPassword) {
        // Retrieve stored user data from SharedPreferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String storedUsername = preferences.getString("username", "");
        String storedPassword = preferences.getString("password", "");

        // Check if entered credentials match stored user data
        return enteredUsername.equals(storedUsername) && enteredPassword.equals(storedPassword);
    }
}
