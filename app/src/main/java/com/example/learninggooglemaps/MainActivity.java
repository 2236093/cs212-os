package com.example.learninggooglemaps;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private PopupWindow popUpWindow;

    private SharedPreferences wmbPreference1;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        ImageView gymDescription = findViewById(R.id.description_equipment);
        ImageView coachesDescription = findViewById(R.id.coaches);





        coachesDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindowCoaches(v);
            }
        });

        gymDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupWindowEquipment(v);
            }
        });

        ImageView exitButton = findViewById(R.id.exitButtonImageView2);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMaps();
            }
        });

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        TextView ratingValue= findViewById(R.id.ratingValue);

        wmbPreference1 = PreferenceManager.getDefaultSharedPreferences(this);

        // Retrieve the saved rating from SharedPreferences
        float savedRating = wmbPreference1.getFloat("userRating", 0.0f);

        // Set the rating to the RatingBar and update the TextView
        ratingBar.setRating(savedRating);
        ratingValue.setText("" + savedRating);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                float previousRating = wmbPreference1.getFloat("userRating", 0.0f);
                int numberOfRatings = wmbPreference1.getInt("numberOfRatings", 0);

                // Calculate the new average rating
                float newRating = (previousRating * numberOfRatings + rating) / (numberOfRatings + 1);

                String formattedRating = String.format("%.2f", newRating);
                float newRatingWithTwoDecimalPlaces = Float.parseFloat(formattedRating);
                // Update the SharedPreferences with the new rating and increment the number of ratings
                editor = wmbPreference1.edit();
                editor.putFloat("userRating", newRatingWithTwoDecimalPlaces);
                editor.putInt("numberOfRatings", numberOfRatings + 1);
                editor.apply();

// Format the new rating with two decimal places


// Update the TextView with the new rating
                ratingValue.setText(formattedRating);
            }
        });

    }

    private void launchMaps() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    private void showPopupWindowEquipment(View anchorView) {
        View popUpView = LayoutInflater.from(this).inflate(R.layout.equipment_window, null);

        popUpWindow = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);

        ImageView exitButton = popUpView.findViewById(R.id.exitButtonImageView);
        ImageView galleryButton = popUpView.findViewById(R.id.gallery_picture);

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGalleryPopupWindow(v, popUpWindow);
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });

        popUpWindow.showAtLocation(anchorView, Gravity.CENTER, 0, 0);
    }

    private void showGalleryPopupWindow(View anchorView, PopupWindow parentPopupWindow) {
        View popUpViewGallery = LayoutInflater.from(this).inflate(R.layout.gallery_layout, null);

        PopupWindow popUpWindowGallery = new PopupWindow(popUpViewGallery, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);

        ImageView exitButtonGallery = popUpViewGallery.findViewById(R.id.exitButtonImageView);

        exitButtonGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindowGallery.dismiss();
            }
        });

        // Show the gallery popup relative to the position of the parent popup
        popUpWindowGallery.showAsDropDown(parentPopupWindow.getContentView());
    }

    private void launchLoginPage() {
        Intent intent = new Intent(this, LoginPage.class);
        startActivity(intent);
    }
    private void showPopupWindowCoaches(View anchorView) {
        View popUpView = LayoutInflater.from(this).inflate(R.layout.coach_window, null);

        popUpWindow = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);

        ImageView exitButton = popUpView.findViewById(R.id.exitButtonImageView);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });
        popUpWindow.showAtLocation(anchorView, Gravity.CENTER, 0, 0);
    }
}

